const path = require('path');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
// const { BundleAnalyzerPlugin } = require('webpack-bundle-analyzer');


module.exports = {
  // devServer: {
  //   https: true,
  // },
  runtimeCompiler: true,
  publicPath: '/',
  assetsDir: '',
  productionSourceMap: false,
  filenameHashing: true,
  parallel: false,
  outputDir: '../express/public',
  css: {
    extract: false,
  },
  configureWebpack: {
    resolve: {
      symlinks: false,
    },
    output: {
      filename: '[name].[hash].js',
      chunkFilename: '[id].[hash].js',
    },
    plugins: [
      new CleanWebpackPlugin(), // 폴더 클린
      // new AsyncChunkNames(), // 스플릿 네임 관리
      // new BundleAnalyzerPlugin(), // 번들 플러그인 분석
    ],
  },
  // pwa: {
  //   name: 'OnAV Business',
  //   themeColor: '#42506b',
  // },
  lintOnSave: false,
  chainWebpack: config => {
    config.module
      .rule('md')
      .test(/\.md/)
      .use('vue-loader')
      .loader('vue-loader')

    config.optimization.minimizer('terser').tap(args => {
      const arg = args;
      arg[0].terserOptions.compress.drop_console = true;
      return arg;
    });

    config.plugin('html').tap(args => {
      const arg = args;
      arg[0].filename = path.resolve('../express/public/index.html');
      return arg;
    });

    config.optimization.splitChunks({
      chunks: 'all',
    });
  },
};
