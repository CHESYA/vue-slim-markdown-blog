import Vue from "vue";
import VueRouter from "vue-router";

Vue.use(VueRouter);

import ArticleEntries from '@/assets/json/Entries.json';

const articleRoutes = Object.keys(ArticleEntries).map(article => {
  const children = ArticleEntries[article].map(child => ({
    path: child.id,
    name: child.id,
    component: () => import(`@/documents/${article}/${child.id}.md`),
  }));

  return {
    path: `/${article}`,
    name: article,
    components: () => import('@/views/Article.vue'),
    children
  };
});

export default new VueRouter({
  mode: 'history',
  // base: process.env.VUE_APP_BASE_URL,
  routes: [
    {
      path: '/',
      name: 'Home',
      component: () => import('@/views/Home.vue'),
    },
    ...articleRoutes,
  ],
});